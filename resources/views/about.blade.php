@extends('layouts.main')

@section('container')
    <h2>{{  $name }}</h2>
    <h3>{{ $email }}</h3>
    <img src="img/{{ $image }}" alt="{{ $name }}" width="200" class="thumbnail rounded-circle">
@endsection