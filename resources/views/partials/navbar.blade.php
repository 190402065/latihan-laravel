<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container">
    <a class="navbar-brand" href="/home">Sistem informasi | Web</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link {{ ($tittle === "Home" ? 'active' : '') }}" href="/home">{{ $a }}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ ($tittle === "About" ? 'active' : '') }}" href="/about">{{ $b }}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ ($tittle === "Blog" ? 'active' : '') }}" href="/blog">{{ $c}}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{ ($tittle === "Hai" ? 'active' : '') }}" href="/hai">{{ $e }}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="https://wa.me/081371929161">{{ $d }}</a>
        </li>
      </ul>
      <form class="d-flex">
      </form>
    </div>
  </div>
</nav>
