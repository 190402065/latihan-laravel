<?php

use App\Http\Controllers\RegaController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;
use App\Models\Post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Di sinilah Anda dapat mendaftarkan rute web untuk aplikasi Anda. Ini
| 	rute dimuat oleh RouteServiceProvider dalam grup yang
| berisi grup middleware "web". Sekarang buat sesuatu yang hebat!
|
*/



Route::get('/', function () {
	return view('welcome');
});
Route::get('/contact', function () {
	return view('contact');
});
Route::get('/menu/{nilai1}/{nilai2}', 'App\Http\Controllers\RegaController@fungsiparameter');
Route::get('/hai', [PostController::class, 'hai']);
Route::get('/', [PostController::class, 'home']);
Route::get('/home', [PostController::class, 'home']);
Route::get('/blog', [PostController::class, 'blog']);
Route::get('/about', [PostController::class, 'about']);
Route::get('/posts', [PostController::class, 'index']);
//halaman single post
Route::get('posts/{slug}', [PostController::class, 'show']);
