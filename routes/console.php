<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| File ini adalah tempat Anda dapat menentukan semua konsol berbasis Penutupan Anda
| perintah. Setiap Penutupan terikat ke instance perintah yang memungkinkan a
| pendekatan sederhana untuk berinteraksi dengan setiap metode IO perintah.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');
