<?php

namespace App\Models;


class Post
{
    private static $blog_posts =
    [
        [
            "tittle" => "Judul Post Pertama",
            "slug" => "judul-post-pertama",
            "author" => "Andra Surya Kurniawan",
            "body" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Debitis corrupti recusandae error laboriosam alias hic beatae dolores asperiores illo commodi deserunt non soluta praesentium porro voluptatibus laborum ab iusto facere, est rerum. Adipisci, exercitationem ut quisquam beatae sit neque nam illum consequatur nesciunt doloremque. Veniam laboriosam labore magnam similique necessitatibus beatae ipsa libero animi porro harum aspernatur nesciunt voluptatibus in, nostrum rem delectus cumque esse rerum exercitationem quo dolor! Possimus modi minima soluta vero placeat in nulla delectus quae itaque ut ipsam doloribus, tempore fugit culpa velit veniam beatae magnam voluptas quasi! Optio cumque in dolore sunt veritatis rem eius?"
        ],

        [
            "tittle" => "Judul Post Kedua",
            "slug" => "judul-post-kedua",
            "author" => "Andi Khairul",
            "body" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Debitis corrupti recusandae error laboriosam alias hic beatae dolores asperiores illo commodi deserunt non soluta praesentium porro voluptatibus laborum ab iusto facere, est rerum. Adipisci, exercitationem ut quisquam beatae sit neque nam illum consequatur nesciunt doloremque. Veniam laboriosam labore magnam similique necessitatibus beatae ipsa libero animi porro harum aspernatur nesciunt voluptatibus in, nostrum rem delectus cumque esse rerum exercitationem quo dolor! Possimus modi minima soluta vero placeat in nulla delectus quae itaque ut ipsam doloribus, tempore fugit culpa velit veniam beatae magnam voluptas quasi! Optio cumque in dolore sunt veritatis rem eius?"
        ]

    ];

    public static function all()
    {
        return collect(self::$blog_posts);
    }

    public static function find($slug)
    {
        $posts = static::all();
        return $posts->firstWhere('slug', $slug);
    }
}
