<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegaController extends Controller
{
    public function fungsiparameter(Request $r)
    {
        return view('viewparam', [
            "tittle" => "Ini latihan pak syahril",
            "judul" => "Ini View parameter"
        ])
            ->with('nilai1', $nil1 = $r->nilai1)
            ->with('nilai2', $nil2 = $r->nilai2);
    }
}
