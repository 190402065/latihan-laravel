<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    public function index()
    {
        return view('posts', [
            "tittle" => "posts",
            "posts" => Post::all()
        ]);
    }

    public function show($slug)
    {
        return view('post', [
            "tittle" => "Single Post",
            "post" => Post::find($slug),
            "a" => "Home",
            "b" => "About",
            "c" => "Blog",
            "d" => "Contact Person",
            "e" => "latihan pak syahril"
        ]);
    }

    public function about()
    {
        return view('about', [
            "tittle" => "About",
            "name" => "Andra surya kurniawan",
            "email" => "andrasurya.100@gmail.com",
            "a" => "Home",
            "b" => "About",
            "c" => "Blog",
            "d" => "Contact Person",
            "image" => "picture.jpeg",
            "e" => "latihan pak syahril"
        ]);
    }

    public function blog()
    {
        $blog_post = [
            [
                "tittle" => "Judul Post Pertama",
                "slug" => "judul-post-pertama",
                "author" => "Andra Surya Kurniawan",
                "body" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Debitis corrupti recusandae error laboriosam alias hic beatae dolores asperiores illo commodi deserunt non soluta praesentium porro voluptatibus laborum ab iusto facere, est rerum. Adipisci, exercitationem ut quisquam beatae sit neque nam illum consequatur nesciunt doloremque. Veniam laboriosam labore magnam similique necessitatibus beatae ipsa libero animi porro harum aspernatur nesciunt voluptatibus in, nostrum rem delectus cumque esse rerum exercitationem quo dolor! Possimus modi minima soluta vero placeat in nulla delectus quae itaque ut ipsam doloribus, tempore fugit culpa velit veniam beatae magnam voluptas quasi! Optio cumque in dolore sunt veritatis rem eius?"
            ],

            [
                "tittle" => "Judul Post Kedua",
                "slug" => "judul-post-kedua",
                "author" => "Andi Khairul",
                "body" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Debitis corrupti recusandae error laboriosam alias hic beatae dolores asperiores illo commodi deserunt non soluta praesentium porro voluptatibus laborum ab iusto facere, est rerum. Adipisci, exercitationem ut quisquam beatae sit neque nam illum consequatur nesciunt doloremque. Veniam laboriosam labore magnam similique necessitatibus beatae ipsa libero animi porro harum aspernatur nesciunt voluptatibus in, nostrum rem delectus cumque esse rerum exercitationem quo dolor! Possimus modi minima soluta vero placeat in nulla delectus quae itaque ut ipsam doloribus, tempore fugit culpa velit veniam beatae magnam voluptas quasi! Optio cumque in dolore sunt veritatis rem eius?"
            ]

        ];
        return view('posts', [
            "tittle" => "Posts",
            "posts" => $blog_post,
            "name" => "Pemrograman Web Framework",
            "a" => "Home",
            "b" => "About",
            "c" => "Blog",
            "d" => "Contact Person",
            "e" => "latihan pak syahril"
        ]);
    }

    public function home()
    {
        return view('home', [
            "tittle" => "Home",
            "name" => "Pemrograman Web Framework",
            "jurusan" => "Sistem Informasi",
            "a" => "Home",
            "b" => "About",
            "c" => "Blog",
            "d" => "Contact Person",
            "e" => "latihan pak syahril"
        ]);
    }

    public function hai()
    {
        return view('hai', [
            "tittle" => "Hai",
            "name" => "Pemrograman Web Framework",
            "jurusan" => "Sistem Informasi",
            "a" => "Home",
            "b" => "About",
            "c" => "Blog",
            "d" => "Contact Person",
            "e" => "latihan pak syahril"
        ]);
    }
}
